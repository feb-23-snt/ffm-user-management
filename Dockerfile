# FROM node:10-alpine
# WORKDIR /app
# ADD package.json /app/package.json
# ADD package-lock.json /app/package-lock.json
# RUN npm install 
# ADD . /app
# EXPOSE 3000
# CMD ["npm", "run", "start:dev"]

FROM node:10-alpine
WORKDIR /app
ADD . /app
RUN npm install 
RUN npm run build
EXPOSE 3000
CMD ["npm", "run", "start:dev"]
